<?xml version="1.0" encoding="UTF-8"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>I, P. 2, Inq. 2, Tract. 1, Q. 1, M. 1, C. 2</title>
        <author ref="#AlexanderOfHales">Alexander of Hales</author>
        <respStmt>
          <name xml:id="JW">Jeffrey C. Witt</name>
          <resp>TEI encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="0.0.0-dev">
          <date when="2017-04-15">April 15, 2017</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <authority>
          <ref target="http://lydiaschumacher.wixsite.com/earlyfranciscans">ERC Project 714427: Authority and Innovation in Early Franciscan Thought</ref>
        </authority>
        <availability status="free">
          <p>Published under a
            <ref target="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License</ref>
          </p>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <listWit>
          <witness xml:id="Q" n="quaracchi1924">Quaracchi 1924</witness>
          <witness xml:id="V" n="vatlat701">Vat lat 701</witness>
          <witness xml:id="B" n="borghlat359">Borgh. lat 359</witness>
          <witness xml:id="U" n="urblat123">Urb lat 123</witness>
          <witness xml:id="Pa" n="bnf15331">BnF 15331</witness>
        </listWit>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
     <schemaRef n="lbp-critical-1.0.0" url="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/master/src/out/critical.rng"/>
     <editorialDecl>
       <p>Encoding of this text has followed the recommendations of the LombardPress 1.0.0
         guidelines for a critical edition.</p>
     </editorialDecl>
   </encodingDesc>
   <revisionDesc status="draft">
     <listChange>
       <change when="2017-04-15" status="draft" n="0.0.0">
         <p>Created file for the first time.</p>
       </change>
     </listChange>
   </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      
      <div xml:id="starts-on">
        <pb ed="#Q" n="517"/><cb ed="#Q" n="a"/>
      </div>
    </front>
    <body>
      <div xml:id="ahsh-l1p2i2t1q1m1c2">
        <head xml:id="ahsh-l1p2i2t1q1m1c2-Hd1e118">I, P. 2, Inq. 2, Tract. 1, Q.1, M. 1, C. 2</head>
        <head xml:id="ahsh-l1p2i2t1q1m1c2-Hd1e121" type="question-title">De nomine 'existentia'</head>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e124">
          <lb ed="#Q"/>Consequenter quaerituri de hoc nomine 'exi<lb ed="#Q"/>stentia ', 
          quod videtur immediate se habere ad
          <lb ed="#Q"/>hoc nomen ' essentia '.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e133">
          <lb ed="#Q"/>I. Quaeritur an sit nomen essentiale aut nomen
          <lb ed="#Q"/>personale.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e140">
          <lb ed="#Q"/>Ad quod sic: 0. Sicut ab eo quod est esse k es<lb ed="#Q"/>sentia,
          ita ab eo quod est existere existentia di<lb ed="#Q"/>citur; 
          sed esse et existere essentialiter dicuntur
          <lb ed="#Q"/>de Deo et idem est Deo existere quod esse;
          <lb ed="#Q"/>ergo existentia essentialiter dicitur de' Deo sicut
          <lb ed="#Q"/>et essentia.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e157">
          <lb ed="#Q"/>b. Item, per abstractionem dicitur'" existentia ad
          <lb ed="#Q"/>existens sicut essentia ad ens; utrumque autem
          <lb ed="#Q"/>essentialiter dicitur et absolute; sed, sicut patet
          <lb ed="#Q"/>ex praedictis 2, nullum tale nomen est appropria<lb ed="#Q"/>bile
          personis.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e170">
          <lb ed="#Q"/>Contra: 1. Richardus de S. Victore, in
          <lb ed="#Q"/>libro De Trinitate 3, dicit": « Quod existere dicitur,
          <lb ed="#Q"/>non solum" intelligitur quod habeat esse, sed
          <lb ed="#Q"/>etiam aliunde, hoc est ex alio, habeat esse ». Si
          <lb ed="#Q"/>ergo esse aliunde vel esse ex alio nullo modo con<lb ed="#Q"/>venit
          essentiae, ergo nullo modo convenit ei exi<lb ed="#Q"/>stere;
          ergo nec nomen existentiae erit essentiale.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e188">
          <lb ed="#Q"/>2. Item, inP intellectu eius quod est ' existentia '
          <lb ed="#Q"/>vel ' existere' intelligitur ratio originis: unde exi<lb ed="#Q"/>stere
          resolutum sonat quasi '! « ex alio sistere 4 »;
          <lb ed="#Q"/>si ergo ' sistere ex alio ' hoc solum convenit per<lb ed="#Q"/>sonae,
          existentia erit personalis et non essentialis.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e201">
          <lb ed="#Q"/>3. Item, convenienter dicuntur tres existentiae
          <lb ed="#Q"/>sicut tres personae; sed nullo modo vere dicitur
          <lb ed="#Q"/>tres essentiae, sed una tantum essentia; relinquitur
          <lb ed="#Q"/>igitur quod existentia non est signiiicativa es<lb ed="#Q"/>sentiae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e214">
          <lb ed="#Q"/>II. Item, quaeritur utrum hoc nomen 'existen—
          <lb ed="#Q"/>tia ' proprius dicatur in divinis quam nomen sub<lb ed="#Q"/>stantiae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e223">
          <lb ed="#Q"/>Ad quod. sic: l. Nomen substantiae ponit esse
          <lb ed="#Q"/>per se, non in alio; sed divinae essentiae magis
          <lb ed="#Q"/>quam creaturae in infinitum convenit esse per se;
          <lb ed="#Q"/>ergo ei magis conveniet nominatio substantiae
          <lb ed="#Q"/>quam creaturae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e237">
          <lb ed="#Q"/>2. Item, existentia ponit esse ex alio; sed esse
          <lb ed="#Q"/>ex alio maxime convenit creaturae; magis igitur
          <lb ed="#Q"/>convenit nomen existentiae creaturae quam Deo;
          <lb ed="#Q"/>ergo de divinis proprius dic-itur nomen substan<lb ed="#Q"/>tiae
          quam existentiae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e250">
          <pb ed="#Q" n="518"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>Contra: a. Richardus, in libro De Trinitatelz
          « Quod increatum est sic consistit in se ipso ut
          <lb ed="#Q"/>nihil ei insit velut in subiectoa ». Si ergo nomen
          <lb ed="#Q"/>substantiae secundum suam propriam rationem
          <lb ed="#Q"/>ex eo dicitur quod substat alicui quod ei inest,
          <lb ed="#Q"/>ergo proprie nomen substantiae non convenit in<lb ed="#Q"/>creatae
          essentiae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e270">
          <lb ed="#Q"/>b. Item, Richardus, in eodem libroz: « Dicimus
          <lb ed="#Q"/>proprietates personis inesse, sed illarum <!--b--> inesse,
          <lb ed="#Q"/>si bene perpendimus, non dat subsistere, sed exi<lb ed="#Q"/>stere
          », hoc est quod una ex alia habeat esse; et
          <lb ed="#Q"/>ideo personae rectius dicuntur existentiae quam
          <lb ed="#Q"/>substantiae vel subsistentiae.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e288">
          <lb ed="#Q"/>Respondeo: I. Dicendum 6, secundum Richar<lb ed="#Q"/>dum,
          in libro De Trinitate 3, quod nomen existen<lb ed="#Q"/>tiae
          significat essentiam cum ordine originis. Unde
          <lb ed="#Q"/>hoc nomen ex sua significatione quantum ad ali<lb ed="#Q"/>quid
          est essentiale, quantum ad aliquid personale.
          <lb ed="#Q"/>Quod declarat Richardus sic: « In discernendis
          <lb ed="#Q"/>personis opus est, ut arbitror, gemina considera<lb ed="#Q"/>tione,
          ut sciamus videlicet et quid sit et unde
          <lb ed="#Q"/>habeat esse. Una istarum considerationum versa<lb ed="#Q"/>tur
          in discernenda reid qualitate, alia vero ver<lb ed="#Q"/>satur
          in investiganda rei origine: ad illam pertinet
          <lb ed="#Q"/>diligenter quaerere quid sit, cum quibus commune,
          <lb ed="#Q"/>quid generale, quid speciale, quid denique pro<lb ed="#Q"/>prium
          assignare naturae; ad istam pertinet sub<lb ed="#Q"/>tiliter
          indagare hoc ipsum quod est unde habeat
          <lb ed="#Q"/>esse, a semetipso an aliunde; et si aliunde quam
          <lb ed="#Q"/>a semetipso, utrum isto vel illoe vel alio quo<lb ed="#Q"/>cumque
          existendi modo. Possumus autemf sub
          <lb ed="#Q"/>nomine existentiae utramque considerationem in<lb ed="#Q"/>telligere,
          tam illam qua quaeritur quale quid sit
          <lb ed="#Q"/>de quolibet ente, tam illam! qua quaeritur unde
          <lb ed="#Q"/>habet esse. Nomen existentiae trahitur ab eo
          <lb ed="#Q"/>quod est existere: in verbo ' sistere ' notari potest
          <lb ed="#Q"/>quod pertinet ad considerationem » qualitatis seu
          <lb ed="#Q"/>quidditatis rei, «per adiunctam praepositionem
          ' ex ', quod pertinet ad aliam », scilicet considera<lb ed="#Q"/>tionem
          originis. Ex hoc ergo colligit4 quod in
          <lb ed="#Q"/>divina proculdubio natura est existentia quae est
          <lb ed="#Q"/>pluribus communis, et sic accipitur essentialiter;
          <lb ed="#Q"/>et est existentia quae est omnino incommunica<lb ed="#Q"/>bilis,
          et sic accipitur personaliter.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e356">
          <lb ed="#Q"/>[Ad obiecta]: a-b. Et per hoc patet solutio ad 
          <lb ed="#Q"/>rationes primo<!--h--> obiectas.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e365">
          <lb ed="#Q"/>1-3. Ad illud vero quod obicitur e contrario
          <lb ed="#Q"/>quod 'in nomine existentiae intelligitur ratio
          <lb ed="#Q"/>originis sive esse ex alio, unde non conveniet
          <lb ed="#Q"/>essentiae ', respondet Richardus5: «Ad con<lb ed="#Q"/>siderationem
          originalis esse pertinet non solum
          <lb ed="#Q"/>quaerere et invenire originem esse ubi est,
          <lb ed="#Q"/>immo etiam quaerere et invenire eam non esse
          <lb ed="#Q"/>ubi non est. Communem autem existentiam
          <lb ed="#Q"/>dicimus ubi intelligitur esse habens ex pro<lb ed="#Q"/>prietate
          communi ; incommunicabilem vero ubi
          <lb ed="#Q"/>intelligitur esse habens ex proprietate incom<lb ed="#Q"/>municabili.
          Tam vere proprium est divinae sub<lb ed="#Q"/>stantiae
          non esse ab aliqua alia substantia, sed
          <lb ed="#Q"/>af se ipsa, quam vere est proprium personae
          <lb ed="#Q"/>originem non habenti, ut. Patri, non esse ab ali<lb ed="#Q"/>qua'f
          alia persona: in uno intelligitur proprietas
          <lb ed="#Q"/>communis, in alterol autem proprietas incom<lb ed="#Q"/>municabilis».
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e408">
          <lb ed="#Q"/>II. 1. Ad illud vero quod consequenter quae<lb ed="#Q"/>ritur
          ' an proprius '" dicatur in divinis nomen exi<lb ed="#Q"/>stentiae
          quam substantiae ': dicendum" quod sic
          <lb ed="#Q"/>et principalius. Nam si respiciamus proprietatem
          <lb ed="#Q"/>dictionis, « in divinis », sicut" dicit Augusti<lb ed="#Q"/>nusG,
          « abusive dicetur substantia», quia pro<lb ed="#Q"/>prietas
          substandi ibi non est, a qua nomen
          <lb ed="#Q"/>substantiae accipitur; existentia vero proprie ibi
          <lb ed="#Q"/>dicetur: existentia enim secundum proprietatem
          <lb ed="#Q"/>dictionis dicit essentiam quasi habitam ab uno
          <lb ed="#Q"/>ex alio vel'P ex uno in alium; et hoc!' convenit
          <lb ed="#Q"/>proprie divino esse, in quo est Filius habens es<lb ed="#Q"/>sentiam
          ex Patre et Spiritus Sanctus habens essen<lb ed="#Q"/>tiam
          ex utroque.
        </p>
        <p xml:id="ahsh-l1p2i2t1q1m1c2-d1e441">
          <lb ed="#Q"/>2. Ad illud vero quod obicitur quod"esse
          <lb ed="#Q"/>ex alio perr prius convenit creaturae ', dicendum
          <lb ed="#Q"/>quod esse ex alio est duobus modis: uno modo
          <lb ed="#Q"/>esse ex alio de eius substantia, hoc modo est esse
          <lb ed="#Q"/>ex alio in divinis personis; alio modo esse ex
          <lb ed="#Q"/>alio ** non de eius substantia, quo modo creaturae
          <lb ed="#Q"/>sunt ex Deo, non de suaf substantia, sed de"
          <lb ed="#Q"/>nihilo. ExV primo notatur ratio originis prout est
          <lb ed="#Q"/>in divinis personis, ex secundo ratio originis
          <lb ed="#Q"/>prout est in creaturis. Prius autem est naturaliter
          <lb ed="#Q"/>esse ex- alio de eius substantia quam esse ex
          <lb ed="#Q"/>alio non de eius substantia; ergo per prius no<lb ed="#Q"/>men
          existentiae conveniet divinis quam crea<lb ed="#Q"/>turis.
        </p>
      </div>
    </body>
  </text>
</TEI>